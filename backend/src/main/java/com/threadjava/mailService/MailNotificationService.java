package com.threadjava.mailService;

import com.threadjava.users.UsersRepository;
import com.threadjava.users.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 13.06.2020
 */
@Service
public class MailNotificationService {

    private final static String POST_LIKE_NOTIFICATION_BODY = "Hello.\n"
            + "Your post was liked by $username.\n"
            + "Post: $link";

    private final static String POST_LIKE_NOTIFICATION_THEME = "Your post liked";

    private final static String SHARE_POST_NOTIFICATION_BODY = "Hello" +
            "$username shared a post with you.\n" +
            "Have a look: $link";

    private final static String SHARE_POST_NOTIFICATION_THEME = "Share post notification";

    private final UsersRepository usersRepository;

    private final JavaMailSender javaMailSender;

    @Value("${frontend.domain.address}")
    private String frontendDomain;

    public MailNotificationService(UsersRepository usersRepository, JavaMailSender javaMailSender) {
        this.usersRepository = usersRepository;
        this.javaMailSender = javaMailSender;
    }

    @Async
    public void sendLikePostNotification(UUID userId, UUID postId) {
        var user = this.getUser(userId);
        var msg = this.createLikePostMailMessage(user, postId.toString());
        javaMailSender.send(msg);
    }

    @Async
    public void sendShareNotification(UUID userId, String postId, String recipientMail) {
        var user = this.getUser(userId);
        var msg = this.createSharePostMailMessage(user.getUsername(), postId, recipientMail);
        javaMailSender.send(msg);
    }

    private SimpleMailMessage createSharePostMailMessage(String userName, String postId, String recipientMail) {
        var result = new SimpleMailMessage();
        var body = createSharePostNotificationBody(userName, postId);
        result.setTo(recipientMail);
        result.setSubject(SHARE_POST_NOTIFICATION_THEME);
        result.setText(body);
        return result;
    }

    private String createSharePostNotificationBody (String userName, String postId) {
        var tokens = new HashMap<String, String>();
            tokens.put("$username", userName);
            tokens.put("$link", this.getPostLink(postId));
        return this.replaceText(SHARE_POST_NOTIFICATION_BODY, tokens);
    }

    private SimpleMailMessage createLikePostMailMessage(User user, String postId) {
        var result = new SimpleMailMessage();
        var body = createLikePostNotificationBody(user.getUsername(), postId);
        result.setTo(user.getEmail());
        result.setSubject(POST_LIKE_NOTIFICATION_THEME);
        result.setText(body);
        return result;
    }

    private String createLikePostNotificationBody(String userName, String postId) {
        var tokens = new HashMap<String, String>();
            tokens.put("$username", userName);
            tokens.put("$link", this.getPostLink(postId));
        return this.replaceText(POST_LIKE_NOTIFICATION_BODY, tokens);
    }

    private String replaceText(String text, Map<String, String> tokens) {
        var result = text;
        for (Map.Entry<String, String> entry: tokens.entrySet()) {
            result = result.replace(entry.getKey(), entry.getValue());
        }
        return result;
    }

    private String getPostLink(String postId) {
        return frontendDomain + "/share/" + postId;
    }

    private User getUser(UUID userId) {
        return usersRepository
                .findById(userId)
                .orElseThrow(() -> new UsernameNotFoundException("No user found with id"));
    }
}
