package com.threadjava.mailService.dto;

import lombok.Data;

import java.util.UUID;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 14.06.2020
 */
@Data
public class SharePostDto {
    private UUID userId;
    private UUID postId;
    private String recipientMail;
}
