package com.threadjava.users;

import com.threadjava.users.dto.UserReactedResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 03.06.2020
 */
@CrossOrigin
@RestController
@RequestMapping("/api/users")
public class UsersController {
    @Autowired
    private UsersService userDetailsService;

    @GetMapping("/liked_post/{postId}")
    public List<UserReactedResponseDto> getLikedPostUsers(@PathVariable UUID postId) {
        return userDetailsService.getPostReactionUserList(postId, true);
    }

    @GetMapping("/disliked_post/{postId}")
    public List<UserReactedResponseDto> getDislikedPostUsers(@PathVariable UUID postId) {
        return userDetailsService.getPostReactionUserList(postId, false);
    }

    @GetMapping("/liked_comment/{commentId}")
    public List<UserReactedResponseDto> getLikedCommentUsers(@PathVariable UUID commentId) {
        return userDetailsService.getCommentReactionUserList(commentId, true);
    }

    @GetMapping("/disliked_comment/{commentId}")
    public List<UserReactedResponseDto> getDislikedCommentUsers(@PathVariable UUID commentId) {
        return userDetailsService.getCommentReactionUserList(commentId, false);
    }
}
