package com.threadjava.users;

import com.threadjava.users.dto.UserListQueryResult;
import com.threadjava.users.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UsersRepository extends JpaRepository<User, UUID> {
    Optional<User> findByEmail(String email);

    Optional<User> findByUsername(String name);

    @Query("SELECT new com.threadjava.users.dto.UserListQueryResult(pr.user.id, pr.user.username, a)"
           + "FROM PostReaction pr LEFT JOIN pr.user.avatar a "
           + "WHERE pr.post.id = :postId AND pr.isLike = :isLike ")
    List<UserListQueryResult> getPostReactionUserList(@Param("postId") UUID postId,
                                                      @Param("isLike") boolean isLike);

    @Query("SELECT new com.threadjava.users.dto.UserListQueryResult(cr.user.id, cr.user.username, a)"
            + "FROM CommentReaction cr LEFT JOIN cr.user.avatar a "
            + "WHERE cr.comment.id = :commentId AND cr.isLike = :isLike ")
    List<UserListQueryResult> getCommentReactionUserList(@Param("commentId") UUID commentId,
                                                         @Param("isLike") boolean isLike);

}