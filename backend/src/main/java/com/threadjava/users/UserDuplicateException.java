package com.threadjava.users;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 19.06.2020
 */
public class UserDuplicateException extends RuntimeException{
    UserDuplicateException() {
        super();
    }

    UserDuplicateException(String msg) {
        super(msg);
    }
}
