package com.threadjava.resetPwd;

import com.threadjava.resetPwd.model.ResetPasswordToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ResetTokenRepository extends JpaRepository<ResetPasswordToken, UUID> {

    @Query("From ResetPasswordToken rt WHERE rt.token = :token AND rt.isDeleted = false")
    Optional<ResetPasswordToken> getByToken(@Param("token") String token);
}