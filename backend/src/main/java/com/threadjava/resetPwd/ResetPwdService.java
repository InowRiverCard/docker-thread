package com.threadjava.resetPwd;

import com.threadjava.auth.TokenService;
import com.threadjava.resetPwd.dto.ResetPwdDto;
import com.threadjava.resetPwd.dto.ResetPwdResponseDto;
import com.threadjava.resetPwd.model.ResetPasswordToken;
import com.threadjava.users.UsersRepository;
import com.threadjava.users.models.User;
import com.threadjava.validator.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class ResetPwdService {
    public static final long EXPIRATION_TIME = 864_000_000; // 1 day

    private final UsersRepository usersRepository;

    private final JavaMailSender javaMailSender;

    private final TokenService tokenService;

    private final ResetTokenRepository resetTokenRepository;

    private final PasswordEncoder bCryptPasswordEncoder;

    private final ResetTokenRepository tokenRepository;

    private final Validator validator;

    @Value("${frontend.domain.address}")
    private String frontendDomain;

    public ResetPwdService(UsersRepository usersRepository,
                           JavaMailSender javaMailSender,
                           TokenService tokenService,
                           ResetTokenRepository resetTokenRepository,
                           PasswordEncoder bCryptPasswordEncoder,
                           ResetTokenRepository tokenRepository,
                           Validator validator) {
        this.usersRepository = usersRepository;
        this.javaMailSender = javaMailSender;
        this.tokenService = tokenService;
        this.resetTokenRepository = resetTokenRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.tokenRepository = tokenRepository;
        this.validator = validator;
    }

    public ResetPwdResponseDto resetPasswordRequest(String email) {
        var result = new ResetPwdResponseDto();
        var validationErrors = validator.validateEmailForResetPwd(email);
        if (validationErrors.isPresent()) {
            throw new ResetPasswordException(validationErrors.get());
        }

        var userOptional = usersRepository.findByEmail(email);
        if (userOptional.isPresent()) {
            var user = userOptional.get();
            var token = this.generateAndSaveToken(user);
            this.sendEmail(user, token);
            result.setMessage("A link to the reset password form, sent to your email!");
        } else {
            throw new ResetPasswordException("This email address does not exist!");
        }
        return result;
    }

    public ResetPwdResponseDto resetPassword(ResetPwdDto resetPwdDto) {
        var validationExceptions = validator.validatePassword(resetPwdDto.getPassword());
        if (validationExceptions.isPresent()) {
            throw new ResetPasswordException(validationExceptions.get());
        }

        var result = new ResetPwdResponseDto();
        var tokenOptional = resetTokenRepository.getByToken(resetPwdDto.getToken());
        if (tokenOptional.isPresent()) {
            var token = tokenOptional.get();
            if (tokenService.isTokenExpired(token.getToken())) {
                throw new ResetPasswordException("Expired token");
            } else {
                var user = token.getUser();
                this.changePassword(user, resetPwdDto.getPassword());
                this.removeToken(token);
                result.setMessage("You changed password");
            }
        } else {
            throw new ResetPasswordException("Not such token in the base");
        }
        return result;
    }

    private void changePassword(User user, String newPassword) {
        user.setPassword(bCryptPasswordEncoder.encode(newPassword));
        usersRepository.save(user);
    }

    private void removeToken(ResetPasswordToken token) {
        token.setIsDeleted(true);
        tokenRepository.save(token);
    }

    private String generateAndSaveToken(User user) {
        String token = tokenService.generatePasswordResetToken(user.getId());
        var resetToken = new ResetPasswordToken(token, user, false);
        this.tokenRepository.save(resetToken);
        return token;
    }

    private void sendEmail(User user, String token) {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(user.getEmail());
        msg.setSubject("Reset Password");
        msg.setText("To complete the password reset process, please click here: "
                + this.createLink(token));
        javaMailSender.send(msg);
    }

    private String createLink(String token) {
        return frontendDomain + "/reset-password/" + token;
    }
}