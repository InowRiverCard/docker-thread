package com.threadjava.post.dto;

import lombok.Data;

import java.util.UUID;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 29.05.2020
 */
@Data
public class PostDeletionResponseDto {
    private UUID postId;
    private UUID userId;
}
