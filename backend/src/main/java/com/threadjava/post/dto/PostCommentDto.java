package com.threadjava.post.dto;

import com.threadjava.users.dto.UserShortDto;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class PostCommentDto {
    private UUID id;
    private String body;
    private UserShortDto user;
    private Date createdAt;
    private Date updatedAt;
    private Boolean isDeleted;
    public long likeCount;
    public long dislikeCount;
}
