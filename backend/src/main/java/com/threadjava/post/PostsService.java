package com.threadjava.post;

import com.threadjava.comment.CommentRepository;
import com.threadjava.comment.dto.CommentDetailsQueryResult;
import com.threadjava.image.ImageRepository;
import com.threadjava.post.dto.*;
import com.threadjava.post.model.Post;
import com.threadjava.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PostsService {
    @Autowired
    private PostsRepository postsCrudRepository;
    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private ImageRepository imageRepository;

    public List<PostListDto> getAllPosts(Integer from, Integer count, UUID userId, String ownPostMode, String likeMode) {
        var pageable = PageRequest.of(from / count, count);
        return postsCrudRepository
                .findAllPosts(userId, ownPostMode, likeMode, pageable)
                .stream()
                .map(PostMapper.MAPPER::postListToPostListDto)
                .collect(Collectors.toList());
    }

    public PostDetailsDto getPostById(UUID id) {
        var post = postsCrudRepository.findPostById(id)
                .map(PostMapper.MAPPER::postToPostDetailsDto)
                .orElseThrow();
        var comments = commentRepository.findCommentsByPostId(id)
                .stream()
                .map(PostMapper.MAPPER::commentToCommentDto)
                .collect(Collectors.toList());
        post.setComments(comments);

        return post;
    }

    public PostCreationResponseDto create(PostCreationDto postDto) {
        Post post = PostMapper.MAPPER.postDetailsDtoToPost(postDto);
        Post postCreated = postsCrudRepository.save(post);
        return PostMapper.MAPPER.postToPostCreationResponseDto(postCreated);
    }

    public PostCreationResponseDto update(PostEditionDto postDto) {
        Post post = postsCrudRepository.findById(postDto.getPostId()).orElseThrow();
        post.setBody(postDto.getBody());
        if (postDto.getImageId() != null) {
            var image = imageRepository.findById(postDto.getImageId()).orElseThrow();
            post.setImage(image);
        }
        Post postUpdated = postsCrudRepository.save(post);
        return PostMapper.MAPPER.postToPostCreationResponseDto(postUpdated);
    }

    public PostDeletionResponseDto delete(PostEditionDto postDto) {
        Post post = postsCrudRepository.findById(postDto.getPostId()).orElseThrow();
        post.setIsDeleted(true);
        Post postUpdated = postsCrudRepository.save(post);
        return PostMapper.MAPPER.postToPostDeletionResponseDto(postUpdated);
    }

    public UUID getPostAuthorId(UUID postId) {
        var post = postsCrudRepository.findPostById(postId)
                .orElseThrow();
        return post.getUser().getId();
    }
}
