package com.threadjava.auth.dto;

import lombok.Data;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 20.06.2020
 */
@Data
public class UserRegisterErrorDto {
    private String email;
    private String username;
    private String password;
}
