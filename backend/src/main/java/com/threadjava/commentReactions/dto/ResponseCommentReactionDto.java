package com.threadjava.commentReactions.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class ResponseCommentReactionDto {
    private UUID commentId;
    private Boolean isLike;
    private Boolean isReverse;
    private UUID userId;
}
