package com.threadjava.comment;

import com.threadjava.comment.dto.*;
import com.threadjava.comment.model.Comment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CommentMapper {
    CommentMapper MAPPER = Mappers.getMapper(CommentMapper.class);

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "post.id", target = "postId")
    CommentCreationResponseDto commentToCommentCreationResponseDto(Comment comment);

    @Mapping(source = "user.avatar", target = "user.image")
    CommentDetailsDto commentToCommentDetailsDto(CommentDetailsQueryResult comment);

    @Mapping(source = "postId", target = "post.id")
    @Mapping(source = "userId", target = "user.id")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "reactions", ignore = true)
    @Mapping(target = "isDeleted", ignore = true)
    Comment commentSaveDtoToModel(CommentSaveDto commentDto);

    @Mapping(source = "postId", target = "post.id")
    @Mapping(source = "userId", target = "user.id")
    @Mapping(source = "commentId", target = "id")
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "reactions", ignore = true)
    @Mapping(target = "isDeleted", ignore = true)
    Comment commentEditDtoToModel(CommentEditionDto commentEditionDto);

    @Mapping(source = "post.id", target = "postId")
    @Mapping(source = "id", target = "commentId")
    CommentDeletionResponseDto commentToPostDeletionResponseDto(Comment model);
}
