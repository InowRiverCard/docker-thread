package com.threadjava.comment.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class CommentEditionDto {
    private String body;
    private UUID commentId;
    private UUID postId;
    private UUID userId;
}
