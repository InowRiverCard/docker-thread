package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsQueryResult;
import com.threadjava.comment.model.Comment;
import com.threadjava.users.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CommentRepository extends JpaRepository<Comment, UUID> {

    @Query("SELECT new com.threadjava.comment.dto.CommentDetailsQueryResult(c.id, c.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "c.createdAt, c.updatedAt, c.user, c.post.id, c.isDeleted) " +
            "FROM Comment c " +
            "WHERE (c.post.id = :postId AND c.isDeleted is false) " +
            "order by c.createdAt desc" )
    List<CommentDetailsQueryResult> findCommentsByPostId(UUID postId);

    @Query("SELECT new com.threadjava.comment.dto.CommentDetailsQueryResult(c.id, c.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "c.createdAt, c.updatedAt, c.user, c.post.id, c.isDeleted) " +
            "FROM Comment c " +
            "WHERE (c.id = :Id) " +
            "order by c.createdAt desc" )
    Optional<CommentDetailsQueryResult> findCommentById(UUID Id);

}