package com.threadjava.events.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 15.06.2020
 */
@AllArgsConstructor
@Data
public class Notification {
    String message;
}
