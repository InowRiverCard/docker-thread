import React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'semantic-ui-react';
import { getUserImgLink } from 'src/helpers/imageHelper';

const PopupUserListItem = ({ user }) => (
  <div>
    <Image src={getUserImgLink(user.image)} avatar size="mini" alt="photo" />
    {user.username}
  </div>
);

PopupUserListItem.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired
};

export default PopupUserListItem;
