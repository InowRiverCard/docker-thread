/* eslint no-param-reassign: "error" */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import { Modal, Button } from 'semantic-ui-react';
import { uploadImage } from 'src/services/imageService';

const ImageCropModal = ({ close, setUserAvatar, src }) => {
  const [imgRef, setImageRef] = useState(undefined);
  const [error, setError] = useState(null);
  const initCrop = {
    x: 10,
    y: 10,
    width: 200,
    height: 200
  };
  const [crop, setCrop] = useState(initCrop);

  const onImageLoaded = image => {
    setImageRef(image);
  };

  const getCroppedImg = (image, localCrop, fileName) => {
    const canvas = document.createElement('canvas');
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = localCrop.width;
    canvas.height = localCrop.height;
    const ctx = canvas.getContext('2d');
    ctx.drawImage(
      image,
      localCrop.x * scaleX,
      localCrop.y * scaleY,
      localCrop.width * scaleX,
      localCrop.height * scaleY,
      0,
      0,
      localCrop.width,
      localCrop.height
    );

    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          reject(new Error('Canvas is empty'));
          return;
        }
        blob.name = fileName;
        resolve(blob);
      }, 'image/jpeg', 1);
    });
  };

  const saveImage = async () => {
    const croppedImage = await getCroppedImg(imgRef, crop, 'avatar');
    try {
      const image = await uploadImage(croppedImage);
      setUserAvatar(image);
      close();
    } catch {
      setError(
        <Modal.Description>
          <p style={{ color: 'red' }}>It failed to upload photo</p>
        </Modal.Description>
      );
    }
  };

  return (
    <Modal centered={false} open onClose={() => close()}>
      <Modal.Content>
        {src && (
          <div>
            <ReactCrop
              circularCrop
              locked
              src={src}
              crop={crop}
              onImageLoaded={onImageLoaded}
              onChange={newCrop => setCrop(newCrop)}
            />
          </div>
        )}
      </Modal.Content>
      {error}
      <Modal.Actions>
        <Button color="red" onClick={close}>
          <span>Cancel</span>
        </Button>
        <Button color="teal" onClick={saveImage}>
          <span>Confirm</span>
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

ImageCropModal.propTypes = {
  close: PropTypes.func.isRequired,
  setUserAvatar: PropTypes.func.isRequired,
  src: PropTypes.string.isRequired
};

export default ImageCropModal;
