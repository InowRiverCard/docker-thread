import React, { useState } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { Form, Button, Segment } from 'semantic-ui-react';

const LoginForm = ({ login, setValidationErrors, errors }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const emailChanged = value => {
    setEmail(value);
    const updError = ({
      ...errors,
      email: undefined
    });
    setValidationErrors(updError);
  };

  const passwordChanged = value => {
    setPassword(value);
    const updError = ({
      ...errors,
      password: undefined
    });
    setValidationErrors(updError);
  };

  const validateEmail = () => {
    if (!validator.isEmail(email)) {
      const updError = ({
        ...errors,
        email: 'Please enter a valid Email'
      });
      setValidationErrors(updError);
    }
  };

  const validatePwd = () => {
    if (!password) {
      const updError = ({
        ...errors,
        password: 'this field is required'
      });
      setValidationErrors(updError);
    }
  };

  const validateInputData = () => {
    validateEmail();
    validatePwd();
  };

  const handleLoginClick = async () => {
    validateInputData();
    const isValid = !errors.email || !errors.password;
    if (!isValid || isLoading) {
      return;
    }
    setIsLoading(true);
    try {
      await login({ email, password });
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Form name="loginForm" size="large" onSubmit={handleLoginClick}>
      <Segment>
        <Form.Input
          fluid
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          error={errors.email}
          onChange={ev => emailChanged(ev.target.value)}
          onBlur={validateEmail}
        />
        <Form.Input
          fluid
          icon="lock"
          iconPosition="left"
          placeholder="Password"
          type="password"
          error={errors.password}
          onChange={ev => passwordChanged(ev.target.value)}
          onBlur={validatePwd}
        />
        <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
          Login
        </Button>
      </Segment>
    </Form>
  );
};

LoginForm.propTypes = {
  login: PropTypes.func.isRequired,
  setValidationErrors: PropTypes.func.isRequired,
  errors: PropTypes.objectOf(PropTypes.any).isRequired
};

export default LoginForm;
