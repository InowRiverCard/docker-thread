import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button } from 'semantic-ui-react';

const UploadAvatarModal = ({ close, setSource }) => {
  const onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener(
        'load',
        () => setSource(reader.result),
        false
      );
      reader.readAsDataURL(e.target.files[0]);
      close();
    }
  };

  return (
    <Modal size="mini" open onClose={close}>
      <Modal.Content>
        <h4>Upload your new Avatar</h4>
      </Modal.Content>
      <Modal.Actions>
        <Button color="red" onClick={close} size="small">Cancel</Button>
        <Button icon as="label" color="teal" size="small">
          <span>Upload image</span>
          <input name="image" type="file" onChange={onSelectFile} hidden />
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

UploadAvatarModal.propTypes = {
  close: PropTypes.func.isRequired,
  setSource: PropTypes.func.isRequired
};

export default UploadAvatarModal;
