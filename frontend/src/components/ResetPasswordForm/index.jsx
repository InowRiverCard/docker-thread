import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Segment, Label, Message } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import validator from 'validator';

const ResetPasswordForm = ({ resetPasswordFunc, token, setMessage, answerMessage }) => {
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPwd] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isPasswordValid, setIsPasswordValid] = useState(true);
  const [isConfirmPwdValid, setIsConfirmPwdValid] = useState(true);
  const [pwdValidMsg, setPwdValidMsg] = useState(undefined);
  const [ConfirmPwdValidMsg, setConfirmPwdValidMsg] = useState(undefined);

  const passwordChanged = data => {
    setPassword(data);
    setIsPasswordValid(true);
    setPwdValidMsg(undefined);
    setMessage({ error: undefined });
  };

  const confirmPasswordChanged = data => {
    setConfirmPwd(data);
    setIsConfirmPwdValid(true);
    setConfirmPwdValidMsg(undefined);
    setMessage({ error: undefined });
  };

  const validateConfirmPwd = async () => {
    const isEqual = validator.equals(password, confirmPassword);
    if (!isEqual) {
      setMessage({ error: 'Passwords must be equal' });
      setIsConfirmPwdValid(false);
      return false;
    }
    setIsConfirmPwdValid(true);
    return true;
  };

  const validatePwd = async () => {
    const isEmpty = validator.isEmpty(password);
    if (isEmpty) {
      setPwdValidMsg('Enter new password');
      setIsPasswordValid(!isEmpty);
      return false;
    }
    const isShort = password.length < 4;
    if (isShort) {
      setPwdValidMsg('Password must be longer then 4 characters');
      setIsPasswordValid(!isShort);
      return false;
    }
    setIsPasswordValid(true);
    return true;
  };

  const validateFields = async () => {
    const isPwdValid = await validatePwd();
    const isConfirmPasswordValid = await validateConfirmPwd();
    return isPwdValid && isConfirmPasswordValid;
  };

  const handleConfirmClick = async () => {
    const isValid = await validateFields();
    if (!isValid || isLoading) {
      return;
    }
    setIsLoading(true);
    try {
      await resetPasswordFunc({ password, token });
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Form name="loginForm" size="large" onSubmit={handleConfirmClick}>
      <Segment>
        <Form.Field>
          <Form.Input
            fluid
            icon="lock"
            iconPosition="left"
            placeholder="Password"
            type="password"
            error={!isPasswordValid}
            onChange={ev => passwordChanged(ev.target.value)}
            onBlur={() => validatePwd()}
          />
          {pwdValidMsg && (
            <Label pointing prompt>
              {pwdValidMsg}
            </Label>
          )}
        </Form.Field>
        <Form.Field>
          <Form.Input
            fluid
            icon="lock"
            iconPosition="left"
            placeholder="Confirm Password"
            type="password"
            error={!isConfirmPwdValid}
            onChange={ev => confirmPasswordChanged(ev.target.value)}
            onBlur={validateConfirmPwd}
          />
          {ConfirmPwdValidMsg && (
            <Label pointing prompt>
              {ConfirmPwdValidMsg}
            </Label>
          )}
        </Form.Field>
        { answerMessage.success && <Message positive>{answerMessage.success}</Message> }
        { answerMessage.error && <Message negative>{answerMessage.error}</Message> }
        { !answerMessage.success && (
          <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
            Confirm
          </Button>
        )}
        { answerMessage.success && <Button as={NavLink} exact to="/login" fluid color="green">Login</Button> }
      </Segment>
    </Form>
  );
};

ResetPasswordForm.propTypes = {
  resetPasswordFunc: PropTypes.func.isRequired,
  token: PropTypes.objectOf(PropTypes.any).isRequired,
  answerMessage: PropTypes.func.isRequired,
  setMessage: PropTypes.func.isRequired
};

export default ResetPasswordForm;
