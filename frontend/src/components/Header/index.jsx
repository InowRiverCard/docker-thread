/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Header as HeaderUI, Image, Grid, Icon, Button } from 'semantic-ui-react';
import StatusEditionModal from '../StatusEditionModal';

import styles from './styles.module.scss';

const Header = ({ user, logout }) => {
  const [StatusModal, toggleEditionModal] = useState(false);

  const handleEditBtn = event => {
    toggleEditionModal(true);
    event.preventDefault();
  };

  return (
    <div className={styles.headerWrp}>
      <Grid centered container columns="2">
        <Grid.Column>
          {user && (
            <NavLink exact to="/">
              <HeaderUI>
                <Image circular src={getUserImgLink(user.image)} />
                <HeaderUI.Content>
                  {user.username}
                  <HeaderUI.Subheader>
                    {user.status}
                    {' '}
                    <Icon name="pencil" size="small" onClick={handleEditBtn} />
                  </HeaderUI.Subheader>
                </HeaderUI.Content>
              </HeaderUI>
            </NavLink>
          )}
        </Grid.Column>
        <Grid.Column textAlign="right">
          <NavLink exact activeClassName="active" to="/profile" className={styles.menuBtn}>
            <Icon name="user circle" size="large" />
          </NavLink>
          <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={logout}>
            <Icon name="log out" size="large" />
          </Button>
        </Grid.Column>
      </Grid>
      {StatusModal
        && (
          <StatusEditionModal
            currentStatus={user.status ? user.status : undefined}
            close={() => toggleEditionModal(false)}
          />
        )}
    </div>
  );
};

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Header;
