import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
import UserPopup from '../UserPopup';
// import UserListItem from '../PopupUserListItem';

import styles from './styles.module.scss';

const Post = ({
  post,
  likePost,
  dislikePost,
  toggleExpandedPost,
  sharePost,
  callEditModal,
  userId,
  callDeleteConfirm,
  setPostLikedUserList,
  setPostDislikedUserList
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    likerList,
    dislikerList
  } = post;
  const date = moment(createdAt).fromNow();

  const likeBtn = () => (
    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
      <Icon name="thumbs up" />
      {likeCount}
    </Label>
  );

  const dislikeBtn = () => (
    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
      <Icon name="thumbs down" />
      {dislikeCount}
    </Label>
  );

  const editBtn = () => {
    let result = null;
    if (userId === user.id) {
      result = (
        <Label
          basic
          size="small"
          as="a"
          className={`${styles.toolbarBtn} ${styles.editBtn}`}
          onClick={() => callEditModal({ id, image, body })}
        >
          <Icon name="edit" />
        </Label>
      );
    }
    return result;
  };

  const deleteBtn = () => {
    let result = null;
    if (userId === user.id) {
      result = (
        <Label
          basic
          size="small"
          as="a"
          className={`${styles.toolbarBtn} ${styles.editBtn}`}
          onClick={() => callDeleteConfirm({ id, image, body })}
        >
          <Icon name="trash" />
        </Label>
      );
    }
    return result;
  };

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <UserPopup
          btn={likeBtn}
          userList={likerList}
          setUserList={setPostLikedUserList}
          id={id}
          disabled={likeCount === 0}
        />
        <UserPopup
          btn={dislikeBtn}
          userList={dislikerList}
          setUserList={setPostDislikedUserList}
          id={id}
          disabled={dislikeCount === 0}
        />
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        {deleteBtn()}
        {editBtn()}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  userId: PropTypes.string.isRequired,
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  callEditModal: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  callDeleteConfirm: PropTypes.func.isRequired,
  setPostLikedUserList: PropTypes.func.isRequired,
  setPostDislikedUserList: PropTypes.func.isRequired
};

export default Post;
