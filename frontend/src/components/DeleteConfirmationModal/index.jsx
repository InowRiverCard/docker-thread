import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button } from 'semantic-ui-react';
import styles from './styles.module.scss';

const ConfirmationWindow = ({ deletedObj, close, deleteFunc, message }) => {
  const handleDeleteBtn = async () => {
    await deleteFunc(deletedObj);
    close();
  };

  return (
    <Modal open onClose={close} size="mini" className={styles.back}>
      <Modal.Header className={styles.header}>
        <h3>{message}</h3>
      </Modal.Header>
      <Modal.Content>
        <div className={styles.centered}>
          <h3>Are you sure?</h3>
        </div>
      </Modal.Content>
      <Modal.Actions>
        <Button color="teal" onClick={handleDeleteBtn}>Yes</Button>
        <Button color="red" floated="left" onClick={close}>No</Button>
      </Modal.Actions>
    </Modal>
  );
};

ConfirmationWindow.propTypes = {
  deleteFunc: PropTypes.func.isRequired,
  deletedObj: PropTypes.objectOf(PropTypes.any).isRequired,
  close: PropTypes.func.isRequired,
  message: PropTypes.string.isRequired
};

export default ConfirmationWindow;
