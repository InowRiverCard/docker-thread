import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Button, Icon, Image } from 'semantic-ui-react';

import styles from './styles.module.scss';

const EditPost = ({ post, close, editPost, uploadImage }) => {
  const [body, setBody] = useState(post.body);
  const [image, setImage] = useState(undefined);
  const [isUploading, setIsUploading] = useState(false);

  const handleAddPost = async () => {
    if (!body) {
      return;
    }
    await editPost({ postId: post.id, imageId: image?.imageId, body });
    close();
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Edit Post</span>
      </Modal.Header>
      <Modal.Content>
        <Form>
          <Form.TextArea
            name="body"
            value={body}
            placeholder="Enter your text"
            onChange={ev => setBody(ev.target.value)}
          />
          {image?.imageLink && (
            <div className={styles.imageWrapper}>
              <Image className={styles.image} src={image?.imageLink} alt="post" />
            </div>
          )}
        </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button
          color="teal"
          icon
          labelPosition="left"
          as="label"
          loading={isUploading}
          disabled={isUploading}
          floated="left"
        >
          <Icon name="image" />
          Attach image
          <input name="image" type="file" onChange={handleUploadFile} hidden />
        </Button>
        <Button color="blue" type="submit" disabled={body === ''} onClick={handleAddPost}>Post</Button>
      </Modal.Actions>
    </Modal>
  );
};

EditPost.propTypes = {
  uploadImage: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired,
  post: PropTypes.shape({ id: PropTypes.string, body: PropTypes.string }).isRequired,
  close: PropTypes.func.isRequired
};

export default EditPost;
