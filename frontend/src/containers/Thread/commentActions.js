import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import * as usersService from 'src/services/usersServise';
import {
  ADD_POST,
  SET_ALL_POSTS,
  SET_EXPANDED_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

const rateComment = (likeDiff, dislikeDiff, id) => (dispatch, getRootState) => {
  const { posts: { expandedPost, expandedPost: { comments } } } = getRootState();
  const mapLikes = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + likeDiff,
    dislikeCount: Number(comment.dislikeCount) + dislikeDiff
  });
  const updated = comments.map(comment => (comment.id !== id ? comment : mapLikes(comment)));

  const updatePost = (post, updComments) => ({
    ...post,
    comments: updComments
  });

  if (expandedPost) {
    dispatch(setExpandedPostAction(updatePost(expandedPost, updated)));
  }
};

export const likeComment = commentId => async (dispatch, getRootState) => {
  const result = await commentService.likeComment(commentId, true);
  const likeDiff = result?.commentId ? 1 : -1;
  const dislikeDiff = result?.isReverse ? -1 : 0;

  rateComment(likeDiff, dislikeDiff, commentId)(dispatch, getRootState);
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
  const result = await commentService.likeComment(commentId, false);
  const dislikeDiff = result?.commentId ? 1 : -1;
  const likeDiff = result?.isReverse ? -1 : 0;

  rateComment(likeDiff, dislikeDiff, commentId)(dispatch, getRootState);
};

export const deleteComment = deletingComment => async (dispatch, getRootState) => {
  const result = await commentService.deleteComment(deletingComment);

  const mapComments = (post, updComments) => ({
    ...post,
    commentCount: Number(post.commentCount) - 1,
    comments: updComments
  });

  const { posts: { posts, expandedPost, expandedPost: { comments } } } = getRootState();
  const updatedComments = comments.filter(comment => (comment.id !== result.commentId));
  const updatedPosts = posts.map(post => (post.id !== result.postId ? post : mapComments(post, updatedComments)));

  dispatch(setPostsAction(updatedPosts));

  if (expandedPost && expandedPost.id === result.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost, updatedComments)));
  }
};

export const editComment = editingComment => async (dispatch, getRootState) => {
  const { id } = await commentService.editComment(editingComment);
  const updatedComment = await commentService.getComment(id);

  const { posts: { expandedPost, expandedPost: { comments } } } = getRootState();
  const updated = comments.map(comment => (comment.id !== updatedComment.id ? comment : updatedComment));

  const updatePost = (post, updComments) => ({
    ...post,
    comments: updComments
  });

  if (expandedPost && expandedPost.id === updatedComment.postId) {
    dispatch(setExpandedPostAction(updatePost(expandedPost, updated)));
  }
};

const ratedUserList = (commentId, users, mapLikers) => async (dispatch, getRootState) => {
  const { posts: { expandedPost, expandedPost: { comments } } } = getRootState();
  const updated = comments.map(comment => (comment.id !== commentId ? comment : mapLikers(comment, users)));
  const updatePost = (post, updComments) => ({
    ...post,
    comments: updComments
  });

  if (expandedPost) {
    dispatch(setExpandedPostAction(updatePost(expandedPost, updated)));
  }
};

export const setCommentLikedUserList = (commentId, isLoadUsers) => async (dispatch, getRootState) => {
  const users = isLoadUsers ? await usersService.getLikedCommentUserList(commentId) : undefined;
  const mapLikers = (comment, userlist) => ({
    ...comment,
    likerList: userlist
  });

  ratedUserList(commentId, users, mapLikers)(dispatch, getRootState);
};

export const setCommentDislikedUserList = (commentId, isLoadUsers) => async (dispatch, getRootState) => {
  const users = isLoadUsers ? await usersService.getCommentDislikedUserList(commentId) : undefined;
  const mapDislikers = (comment, userlist) => ({
    ...comment,
    dislikerList: userlist
  });

  ratedUserList(commentId, users, mapDislikers)(dispatch, getRootState);
};

