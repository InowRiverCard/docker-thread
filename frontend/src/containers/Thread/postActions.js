import * as postService from 'src/services/postService';
import * as usersService from 'src/services/usersServise';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const deletePost = post => async (dispatch, getRootState) => {
  const deletingPost = { postId: post.id, imageId: post.image?.imageId, body: post.body };
  const result = await postService.deletePost(deletingPost);

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.filter(p => (p.id !== result.postId));

  dispatch(setPostsAction(updated));
  if (expandedPost && expandedPost.id === result.postId) {
    dispatch(setExpandedPostAction(undefined));
  }
};

export const editPost = editedPost => async (dispatch, getRootState) => {
  const { id } = await postService.editPost(editedPost);
  const updatedPost = await postService.getPost(id);

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== updatedPost.id ? post : updatedPost));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === updatedPost.id) {
    dispatch(setExpandedPostAction(updatedPost));
  }
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

const ratePost = (likeDiff, dislikeDiff, postId) => (dispatch, getRootState) => {
  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + likeDiff,
    dislikeCount: Number(post.dislikeCount) + dislikeDiff
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const likePost = postId => async (dispatch, getRootState) => {
  const result = await postService.likePost(postId);
  const likeDiff = result?.id ? 1 : -1;
  const dislikeDiff = result?.isReverse ? -1 : 0;

  ratePost(likeDiff, dislikeDiff, postId)(dispatch, getRootState);
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const result = await postService.dislikePost(postId);
  const dislikeDiff = result?.id ? 1 : -1;
  const likeDiff = result?.isReverse ? -1 : 0;

  ratePost(likeDiff, dislikeDiff, postId)(dispatch, getRootState);
};

const ratedUserList = (postId, users, mapLikes) => async (dispatch, getRootState) => {
  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post, users)));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost, users)));
  } else {
    dispatch(setPostsAction(updated));
  }
};

export const setPostLikedUserList = (postId, isLoadUsers) => async (dispatch, getRootState) => {
  const users = isLoadUsers ? await usersService.getLikedPostUserList(postId) : undefined;
  const mapUsers = (post, userlist) => ({
    ...post,
    likerList: userlist
  });

  ratedUserList(postId, users, mapUsers)(dispatch, getRootState);
};

export const setPostDislikedUserList = (postId, isLoadUsers) => async (dispatch, getRootState) => {
  const users = isLoadUsers ? await usersService.getdislikedPostUserList(postId) : undefined;

  const mapUsers = (post, userlist) => ({
    ...post,
    dislikerList: userlist
  });

  ratedUserList(postId, users, mapUsers)(dispatch, getRootState);
};

export const sharePostByEmail = request => postService.shareByEmail(request);
