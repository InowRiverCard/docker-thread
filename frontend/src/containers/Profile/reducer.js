import {
  SET_USER,
  SET_VALIDATION_ERRORS,
  SET_RESET_PWD_MESSAGE,
  SET_IS_UPDATE_SUCCESS
} from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        user: action.user,
        isAuthorized: Boolean(action.user?.id),
        isLoading: false
      };
    case SET_VALIDATION_ERRORS:
      return {
        ...state,
        validationErrors: action.errors
      };
    case SET_RESET_PWD_MESSAGE:
      return {
        ...state,
        resetPasswordMessge: action.message
      };
    case SET_IS_UPDATE_SUCCESS:
      return {
        ...state,
        isUpdateSuccess: action.isSuccess
      };
    default:
      return state;
  }
};
