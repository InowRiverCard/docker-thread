/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Grid, Image, Input, Button, Label } from 'semantic-ui-react';
import ImageCropModal from 'src/components/ImageCropModal';
import StatusEditionModal from 'src/components/StatusEditionModal';
import UploadAvatarModal from 'src/components/UploadAvatarModal';
import UpdateUserPropertyModal from 'src/components/UpdateUserPropertyModal';
import { setUserImage, updateUserEmail, setValidationErrors, updateUserName, setUpdateSuccess } from './actions';

const Profile = ({
  user,
  updateUserName: changeUserName,
  setUserImage: changeAvatar,
  updateUserEmail: changeEmail,
  validationErrors,
  setValidationErrors: setErrors,
  isUpdateSuccess,
  setUpdateSuccess: setUpdSuccess
}) => {
  const [uploadAvatarModal, setAvatarModal] = useState(false);
  const [statusModal, toggleEditionModal] = useState(false);
  const [src, setSource] = useState(undefined);

  return (
    <div>
      <Grid container textAlign="center" style={{ paddingTop: 30 }}>
        <Grid.Row>
          <Grid.Column>
            <Image centered src={getUserImgLink(user.image)} size="medium" circular />
            <br />
            <Button basic color="grey" onClick={() => setAvatarModal(true)}>
              <span>Update img</span>
            </Button>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={3}>
            <Input
              icon="user"
              iconPosition="left"
              placeholder="Username"
              type="text"
              disabled
              value={user.username}
            />
          </Grid.Column>
          <Grid.Column width={1}>
            <UpdateUserPropertyModal
              header="Enter new name"
              setProperty={changeUserName}
              type="text"
              defaultValue={user.username}
              validationError={validationErrors.username}
              setError={setErrors}
              isSuccess={isUpdateSuccess.isSuccess}
              setUpdateSuccess={setUpdSuccess}
              propertyName="Name"
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={3}>
            <Input
              icon="at"
              iconPosition="left"
              placeholder="Email"
              type="email"
              disabled
              value={user.email}
            />
          </Grid.Column>
          <Grid.Column width={1}>
            <UpdateUserPropertyModal
              header="Write new Email"
              setProperty={changeEmail}
              type="email"
              defaultValue={user.email}
              validationError={validationErrors.email}
              setError={setErrors}
              isSuccess={isUpdateSuccess.isSuccess}
              setUpdateSuccess={setUpdSuccess}
              propertyName="Email"
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={8} />
          <Grid.Column width={8} textAlign="left">
            <Label color="yellow" as="a" onClick={() => toggleEditionModal(true)}>
              {user.status ? user.status : 'add status'}
            </Label>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      {uploadAvatarModal && <UploadAvatarModal close={() => setAvatarModal(false)} setSource={setSource} />}
      {src && (
        <ImageCropModal
          user={user}
          close={() => setSource(undefined)}
          src={src}
          setUserAvatar={changeAvatar}
        />
      )}
      {statusModal
        && (
          <StatusEditionModal
            currentStatus={user.status ? user.status : undefined}
            close={() => toggleEditionModal(false)}
          />
        )}
    </div>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  setUserImage: PropTypes.func.isRequired,
  updateUserEmail: PropTypes.func.isRequired,
  validationErrors: PropTypes.objectOf(PropTypes.any),
  setValidationErrors: PropTypes.func.isRequired,
  updateUserName: PropTypes.func.isRequired,
  isUpdateSuccess: PropTypes.objectOf(PropTypes.any),
  setUpdateSuccess: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {},
  validationErrors: {},
  isUpdateSuccess: {}
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user,
  validationErrors: rootState.profile.validationErrors,
  isUpdateSuccess: rootState.profile.isUpdateSuccess
});

const actions = {
  setUserImage,
  updateUserEmail,
  setValidationErrors,
  updateUserName,
  setUpdateSuccess
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
