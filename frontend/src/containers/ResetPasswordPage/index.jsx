import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Logo from 'src/components/Logo';
import { Grid, Header, Message } from 'semantic-ui-react';
import { resetPassword, setResetPasswordMessage } from 'src/containers/Profile/actions';
import ResetPasswordForm from 'src/components/ResetPasswordForm';
import { NavLink } from 'react-router-dom';

const ResetPwdPage = ({ match, resetPassword: resetPwd, setResetPasswordMessage: setMessage, resetPasswordMessge }) => (
  <Grid textAlign="center" verticalAlign="middle" className="fill">
    <Grid.Column style={{ maxWidth: 450 }}>
      <Logo />
      <Header as="h2" color="teal" textAlign="center">
        Enter New password
      </Header>
      <ResetPasswordForm
        resetPasswordFunc={resetPwd}
        token={match.params.token}
        setMessage={setMessage}
        answerMessage={resetPasswordMessge}
      />
      <Message>
        New to us?
        {' '}
        <NavLink exact to="/registration">Sign Up</NavLink>
      </Message>
    </Grid.Column>
  </Grid>
);

ResetPwdPage.propTypes = {
  match: PropTypes.objectOf(PropTypes.any),
  resetPassword: PropTypes.func.isRequired,
  setResetPasswordMessage: PropTypes.func.isRequired,
  resetPasswordMessge: PropTypes.objectOf(PropTypes.any)
};

ResetPwdPage.defaultProps = {
  match: undefined,
  resetPasswordMessge: {}
};

const mapStateToProps = rootState => ({
  validationErrors: rootState.profile.validationErrors,
  resetPasswordMessge: rootState.profile.resetPasswordMessge
});

const actions = { resetPassword, setResetPasswordMessage };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResetPwdPage);
