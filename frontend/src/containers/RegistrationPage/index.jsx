import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { register, setValidationErrors } from 'src/containers/Profile/actions';
import Logo from 'src/components/Logo';
import { Grid, Header, Message } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import RegistrationForm from 'src/components/RegistrationForm';

const RegistrationPage = ({ register: signOn, validationErrors, setValidationErrors: setErrors }) => (
  <Grid textAlign="center" verticalAlign="middle" className="fill">
    <Grid.Column style={{ maxWidth: 450 }}>
      <Logo />
      <Header as="h2" color="teal" textAlign="center">
        Register for free account
      </Header>
      <RegistrationForm registration={signOn} errors={validationErrors} setValidationErrors={setErrors} />
      <Message>
        Alredy with us?
        {' '}
        <NavLink exact to="/login">Sign In</NavLink>
      </Message>
    </Grid.Column>
  </Grid>
);

RegistrationPage.propTypes = {
  register: PropTypes.func.isRequired,
  setValidationErrors: PropTypes.func.isRequired,
  validationErrors: PropTypes.objectOf(PropTypes.any)
};

RegistrationPage.defaultProps = {
  validationErrors: {}
};

const mapStateToProps = rootState => ({
  validationErrors: rootState.profile.validationErrors
});

const actions = { register, setValidationErrors };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegistrationPage);
